<?php defined ( 'ABSPATH' ) || exit; ?>
<div class="wrap gusta-demo-wrap">
    <h2><?php _e('Demo Importer', 'gusta-demo'); ?></h2>

    <form method="post" id="purchase-code-status" onsubmit="return false;">
        <div class="section">
            <h3><?php echo _e('Verify your Envato Purchase'); ?></h3>
            <p><?php echo _e('In order to be able to import the demos listed below, you need to verify your Envato purchase.', 'mb_framework'); ?> <a target="_blank" href="https://help.market.envato.com/hc/en-us/articles/202822600-Where-Is-My-Purchase-Code-"><?php echo _e('Where is my purchase code?', 'mb_framework'); ?></a></p>
            <div class="verified-status" <?php echo ! $verified ? 'style="display:none"' : ''; ?>>
                <p>
                    <span><?php printf(__('<strong>Verified:</strong> <span class="echo-purchase-code">%s</span>', 'gusta-demo'), GustaDemo\Admin::getSetting('purchase_code')); ?></span>
                    <span>&mdash; <a href="javascript:;"><?php _e('Enter a new purchase code', 'gusta-demo'); ?></a></span>
                </p>
            </div>

            <div class="input-hold" <?php echo $verified ? 'style="display:none"' : ''; ?>>
                <input type="text" name="purchase-code" value="<?php echo esc_attr( GustaDemo\Admin::getSetting('purchase_code') ); ?>" />
                <input type="submit" value="<?php echo esc_attr_e('Verify', 'gusta-demo'); ?>" data-loading="<?php echo esc_attr_e('Verifying..', 'gusta-demo'); ?>" class="button button-primary" />
                <input type="hidden" name="action" value="gusta-demo-verify-purchase-code" />
                <?php wp_nonce_field('gusta-demo-verify', 'nonce'); ?>
            </div>
            
        </div>
    </form>
</div>

<?php if ( class_exists('WP_Importer') ) : ?>

    <div class="wrap gusta-demo-grid-wrap">
        <h2><?php _e('Demos', 'gusta-demo'); ?></h2>
        
        <div class="container">
            <div class="demos-grid row">
                
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-1.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 1 - Dark Background.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-1" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-2.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 2 - with Dark Background 2.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-2" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-3.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 3 - Flat 1.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-3" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-4.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 4 - Overlay 3.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-4" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-5.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 5 - Flat 2.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-5" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-6.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 6 - Overlay 1.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-6" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-7.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 7 - Overlay 2.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-7" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-8.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 8 - without featured image.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-8" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-9.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 9 - Horizontal 2.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-9" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-10.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 10 - Horizontal Large Layout.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-10" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-11.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 11 - Timeline.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-11" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-12.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 12 - Basic.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-12" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-13.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 13 - Material.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-13" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-14.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 14 - for Sidebar.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-14" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
                <div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-15.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 15 - Overlay 4.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-15" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-16.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 16 - Dark Overlay Read More on Hover.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-16" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-17.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 17 - Sky Blue Overlay on Hover.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-17" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-18.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 18 - 1 Column Simple with Author.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-18" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-19.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 19 - Absolute Radius.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-19" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-20.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 20 - Ticket Purple Line on Hover.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-20" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-21.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 21 - Dark BG Image on Hover.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-21" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-22.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 22 - Obscure with Hover.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-22" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-23.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 23 - Crypto Dark Blue Background.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-23" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-24.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 24 - Cloud Degrade Framed Image on Hover.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-24" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-25.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 25 - Purple Green Hover.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-25" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-26.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 26 - Dark BG - Anim Author.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-26" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-27.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 27 - White Description Overlay.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-27" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-28.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 28 - Flat Contrast.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-28" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-29.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 29 - Elegant Border.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-29" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-30.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 30 - Picture of the Day.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-30" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-31.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 31 - Flat Border Bottom.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-31" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-32.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 32 - Dark Hover Overlay.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-32" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-33.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 33 - Gradient Hover Overlay - Learn More.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-33" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-34.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 34 - Dark Overlay - Hover Category and Date.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-34" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-35.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 35 - Flat - Hover Shadow Up.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-35" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-36.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 36 - Padded Border - White Overlay.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-36" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-37.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 37 - Dark Overlay - Text Center.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-37" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-38.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 38 - Overlay Text on Hover - Circle Button.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-38" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-39.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 39 - Full Width Overlay Text and Social.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-39" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
				
				<div class="demo-item col-6">
                    <div class="inside">
                        <div class="demo-img">
                            <img src="https://themegusta.com/showcase/previews/post-listing-demo-40.jpg" />
                            <span class="demo-title-hold"></span>
                        </div>

                        <div class="demo-actions">
                            <button type="button" class="button button-primary <?php echo ! $verified ? 'not-verified" disabled="disabled' : 'verified'; ?>" data-demo="./post listing/Smart Sections Post Listing Demo 40 - White BG Padding on Hover.xml">
                                <?php echo ! $verified ? __('Verify purchase') : __('Import', 'gusta-demo'); ?>
                            </button>
                            <a href="https://www.themegusta.com/showcase-grid/#post-listing-demo-40" target="_blank" class="button"></a>
                        </div>
                    </div>
                </div>
 
            </div>
        </div>
    </div>

<?php endif; ?>