<?php
/*
* Dynamic CSS Functions
*
*
* @file           includes/css.php
* @package        Smart Sections
* @author         Bora Demircan & Ali Metehan Erdem
* @copyright      2017 Theme Gusta
* @license        license.txt
* @version        Release: 1.0.0
*
*/

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/* Displays the inline css of shortcodes */
if ( !function_exists( 'gusta_inline_shortcode_css' ) ):
	function gusta_inline_shortcode_css ( $dynamic_css, $post_id=null, $type=null, $card_design_class=null ) {
		global $post;
		
		$preview_id = $content = '';
		
		if ($type=='section-inner'):
			$content = get_post_field('post_content', $post_id);
		elseif ($type=='section'):
			$content = gusta_get_section_var ($post_id, 'post_content');
		else:
			if ($post):
				if (is_singular()):
					$content = $post->post_content;
				endif;
			endif;
		endif;
		
		if ($post_id==''): $post_id = $preview_id; endif;
		
		$shortcodes = array (
			'gusta_social_sharing_box',
			'gusta_post_listing',
			'gusta_taxonomy_listing',
			'gusta_post_carousel',
			'gusta_post_title',
			'gusta_post_excerpt',
			'gusta_post_date',
			'gusta_post_content',
			'gusta_post_author',
			'gusta_post_author_image',
			'gusta_post_categories',
			'gusta_post_tags',
			'gusta_post_taxonomies',
			'gusta_post_read_more_button',
			'gusta_post_custom_field',
			'gusta_text',
			'gusta_container',
			'gusta_post_featured_image',
			'gusta_post_featured_image_as_background',
		);
		
		if (!$dynamic_css): $dynamic_css = array(); endif;
		
		if ($content!=''):
		
			$r = $content;
			
			foreach ($shortcodes as $shortcode):
				if (strpos($r, "[".$shortcode) !== false):
					
					$loop=true;
					$i=0;
					while ($loop==true):
						$atts=array();
						$shatts = gusta_get_string_between($r, '['.$shortcode.' ', ']');
						$atts = shortcode_parse_atts($shatts);
						
						$r = str_replace('['.$shortcode.' '.$shatts.']', '', $r);
						$r = str_replace('['.$shortcode.']', '', $r);
						
						if ($shatts): 
							extract($atts); 
							
							if (file_exists(SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/css/'.$shortcode.'.php')):
								include(SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/css/'.$shortcode.'.php');
							endif;
							
						unset ($atts);
						endif;
						
						if (strpos($r, "[".$shortcode) === false):
							$loop=false;
						endif;
						$i++;
						if ($i==50) : $loop=false; endif;
					
					endwhile;
				endif;
			endforeach;
		endif;
		wp_reset_query();
		return ($dynamic_css);
	}
endif;


/* This function parses all the inline dynamic css of shortcode inside content of the page and the included sections. */
if ( !function_exists( 'gusta_parse_dynamic_css' ) ):
	function gusta_parse_dynamic_css () {
		$parse_css = $sections_css = '';
		$dynamic_css = array();
		$dynamic_css = gusta_inline_shortcode_css ( $dynamic_css );
		if (!(is_preview() && (get_post_type()=="gusta_section"))):
		
		foreach ($dynamic_css as $var => $val):
			if ($val): $parse_css .= ' '. $var . ' { ' . $val . ' }'; endif;
		endforeach;				
		
		$parse_css .= $sections_css;
		
		$parse_css = trim ( preg_replace( '/\s+/', ' ', $parse_css) );

		if ($parse_css!=''): echo '
<style id="gusta_inline_css">'.$parse_css.'</style>
'; endif; endif;
	}
	add_action ( 'wp_head', 'gusta_parse_dynamic_css', 9999 );
endif;

/* Parses the dynamic CSS shortcode attributes */
if ( !function_exists( 'gusta_show_dynamic_css' ) ):
	function gusta_show_dynamic_css ( $atts ) {
		/*
		'el_class' => '',
		'dynamic_css' => '',
		'shatts' => '',
		'el_slug' => '',
		'text_class' => '',
		'text_active_class' => '',
		'enable_hover' => '',
		'hover_class' => ''
		'enable_active' => '',
		'active_class' => ''
		'after_class' => ''
		'hover_after_class' => ''
		'active_after_class' => '' 
		*/

		extract ($atts);
		if ($el_class!='#'):

			if (!isset($shatts['tg_'.$el_slug."_tg_normal_tg_advanced_css"])): $shatts['tg_'.$el_slug."_tg_normal_tg_advanced_css"]=""; endif;
			if (!isset($shatts['tg_'.$el_slug."_tg_normal_tg_background_image"])): $shatts['tg_'.$el_slug."_tg_normal_tg_background_image"]=""; endif;
			if (!isset($dynamic_css[$el_class])): $dynamic_css[$el_class]=""; endif;
			
			$el_css = $shatts['tg_'.$el_slug."_tg_normal_tg_advanced_css"];
			
			$bg=false;
			if ($el_slug=='container'): $bg = true; endif;
			
			$el_css = gusta_box_shadow_css ($el_css);
			$el_css = gusta_background_css ($el_css, $shatts['tg_'.$el_slug."_tg_normal_tg_background_image"], $bg);
			
			if ($el_css!=""): ($dynamic_css[$el_class] ? $dynamic_css[$el_class] .= $el_css : $dynamic_css[$el_class] = $el_css); endif;
			
			if (strpos($shatts['tg_'.$el_slug."_tg_normal_tg_advanced_css"], 'overlay') !== false) {
				if (!isset($overlay_class)): $overlay_class = $el_class.' .gusta-overlay'; endif;
				$overlay_css = gusta_overlay_css ($shatts['tg_'.$el_slug."_tg_normal_tg_advanced_css"]);
				$dynamic_css[$overlay_class]=$overlay_css;
			}

			if ($enable_hover):
				if (!isset($shatts['tg_'.$el_slug."_tg_hover_tg_advanced_css"])) { $shatts['tg_'.$el_slug."_tg_hover_tg_advanced_css"]=""; }
				if (!isset($shatts['tg_'.$el_slug."_tg_hover_tg_background_image"])) { $shatts['tg_'.$el_slug."_tg_hover_tg_background_image"]=""; }
				
				if (!isset($hover_class)): $hover_class=""; endif;
				
				$el_hover_class = ($hover_class!='' ? $hover_class : $el_class.':hover');
				
				if (!isset($dynamic_css[$el_hover_class])): $dynamic_css[$el_hover_class]=""; endif;
				
				$el_hover_css = $shatts['tg_'.$el_slug."_tg_hover_tg_advanced_css"];
				$el_hover_css = gusta_box_shadow_css ($el_hover_css);
				$el_hover_css = gusta_background_css ($el_hover_css, $shatts['tg_'.$el_slug."_tg_hover_tg_background_image"], $bg);
				
				if ($el_hover_css!=""): ($dynamic_css[$el_hover_class] ? $dynamic_css[$el_hover_class] .= $el_hover_css : $dynamic_css[$el_hover_class] = $el_hover_css); endif;
				
				if (strpos($shatts['tg_'.$el_slug."_tg_hover_tg_advanced_css"], 'overlay') !== false) {
					if (!isset($overlay_hover_class)): $overlay_hover_class = $el_hover_class.' .gusta-overlay'; endif;
					$overlay_hover_css = gusta_overlay_css ($shatts['tg_'.$el_slug."_tg_hover_tg_advanced_css"]);
					$dynamic_css[$overlay_hover_class]=$overlay_hover_css;
				}
			endif;

			if ($enable_active):
				if (!isset($shatts['tg_'.$el_slug."_tg_active_tg_advanced_css"])) { $shatts['tg_'.$el_slug."_tg_active_tg_advanced_css"]=""; }
				if (!isset($shatts['tg_'.$el_slug."_tg_active_tg_background_image"])) { $shatts['tg_'.$el_slug."_tg_active_tg_background_image"]=""; }
				
				$el_active_class = $active_class;
				
				if (!isset($dynamic_css[$el_active_class])): $dynamic_css[$el_active_class]=""; endif;
				
				$el_active_css = $shatts['tg_'.$el_slug."_tg_active_tg_advanced_css"];
				$el_active_css = gusta_box_shadow_css ($el_active_css);
				$el_active_css = gusta_background_css ($el_active_css, $shatts['tg_'.$el_slug."_tg_active_tg_background_image"]);
				
				if ($el_active_css!=""): ($dynamic_css[$el_active_class] ? $dynamic_css[$el_active_class] .= $el_active_css : $dynamic_css[$el_active_class] = $el_active_css); endif;
			endif;
		endif;

		return $dynamic_css;
	}
endif;

/* Box Shadow CSS */
if ( !function_exists( 'gusta_box_shadow_css' ) ):
	function gusta_box_shadow_css ($advanced_css) {
		$box_shadow = gusta_get_string_between ($advanced_css, 'box-shadow:', ' !important;');
		$shadow_color = gusta_get_string_between ($advanced_css, 'box-shadow-color:', ' !important;');
		if ($shadow_color && $box_shadow):
			$new_box_shadow = $box_shadow.' '.$shadow_color;
			$newcss = str_replace ('box-shadow-color:'.$shadow_color.' !important;','',$advanced_css);
			$newcss = str_replace ('box-shadow:'.$box_shadow.' !important;','box-shadow:'.$new_box_shadow.' !important;',$newcss);
			return $newcss;
		else:
			return $advanced_css;
		endif;
	}
endif;

/* Overlay CSS*/
if ( !function_exists( 'gusta_overlay_css' ) ):
	function gusta_overlay_css ($advanced_css) {
		$overlay = gusta_get_string_between ($advanced_css, 'overlay:', ' !important;');
		$gradient_direction = gusta_get_string_between ($advanced_css, 'gradient_direction:', ' !important;');
		if (!$gradient_direction): $gradient_direction='top'; endif;
		$percentage_from = gusta_get_string_between ($advanced_css, 'gradient_percentage_from:', ' !important;');
		if (!$percentage_from): $percentage_from='0%'; endif;
		$percentage_to = gusta_get_string_between ($advanced_css, 'gradient_percentage_to:', ' !important;');
		if (!$percentage_to): $percentage_to='100%'; endif;
		$overlay_color = gusta_get_string_between ($advanced_css, 'overlay-color:', ' !important;');
		$gradient_color = gusta_get_string_between ($advanced_css, 'gradient-color:', ' !important;');
		
		$return = '';
		
		if ($overlay_color):
			$return .= 'background:';
			$type = 'linear-gradient';
			switch ($gradient_direction) {
				case 'top': $direction = 'to bottom'; break;
				case 'left': $direction = 'to right'; break;
				case 'top-left': $direction = '135deg'; break;
				case 'bottom-left': $direction = '45deg'; break;
				case 'radial': $direction = 'ellipse at center'; $type = 'radial-gradient'; break;
			}
			
			if (!$direction): $direction='to bottom'; endif;
			if (!$overlay_color): $overlay_color = 'rgba(255,255,255,0)'; endif;
			if (!$gradient_color): $gradient_color = $overlay_color; endif;
			
			$return .= ' '.$type.'('.$direction.', '.gusta_hextorgbcss($overlay_color, 1).' '.$percentage_from.', '.gusta_hextorgbcss($gradient_color, 1).' '.$percentage_to.')';
			$return .= ' !important;';
		endif;
		
		return $return;
	}
endif;

/* Background CSS */
if ( !function_exists( 'gusta_background_css' ) ):
	function gusta_background_css ($advanced_css, $background_image, $bg=false) {
		$bg_style = gusta_get_string_between ($advanced_css, 'background-style:', ' !important;');
		$return = str_replace("background-style:".$bg_style." !important;","",$advanced_css);
		if ($bg_style=="cover"):
			$return .= 'background-size:cover !important; background-repeat:no-repeat !important;';
		elseif ($bg_style=="contain"):
			$return .= 'background-size:contain !important; background-repeat:no-repeat !important;';
		elseif ($bg_style=="no-repeat"):
			$return .= 'background-repeat:no-repeat !important;';
		elseif ($bg_style=="repeat"):
			$return .= 'background-repeat:repeat !important;';
		endif;
		$overlay = gusta_get_string_between ($advanced_css, 'overlay:', ' !important;');
		$gradient_direction = gusta_get_string_between ($advanced_css, 'gradient_direction:', ' !important;');
		if (!$gradient_direction): $gradient_direction='top'; endif;
		$percentage_from = gusta_get_string_between ($advanced_css, 'gradient_percentage_from:', ' !important;');
		if (!$percentage_from): $percentage_from='0%'; endif;
		$percentage_to = gusta_get_string_between ($advanced_css, 'gradient_percentage_to:', ' !important;');
		if (!$percentage_to): $percentage_to='100%'; endif;
		$overlay_color = gusta_get_string_between ($advanced_css, 'overlay-color:', ' !important;');
		$gradient_color = gusta_get_string_between ($advanced_css, 'gradient-color:', ' !important;');
		$bg_return = '';
		
		if ($overlay_color!='' || $background_image):
			$bg_return .= ' background-image:';
			if ($overlay_color && $bg==false):
				$type = 'linear-gradient';
				switch ($gradient_direction) {
					case 'top': $direction = 'to bottom'; break;
					case 'left': $direction = 'to right'; break;
					case 'top-left': $direction = '135deg'; break;
					case 'bottom-left': $direction = '45deg'; break;
					case 'radial': $direction = 'ellipse at center'; $type = 'radial-gradient'; break;
				}
				
				if (!$direction): $direction='to bottom'; endif;
				if (!$overlay_color): $overlay_color = 'rgba(255,255,255,0)'; endif;
				if (!$gradient_color): $gradient_color = $overlay_color; endif;
				
				$bg_return .= ' '.$type.'('.$direction.', '.gusta_hextorgbcss($overlay_color, 1).' '.$percentage_from.', '.gusta_hextorgbcss($gradient_color, 1).' '.$percentage_to.')';
			endif;
			
			if ($background_image):
				if ($overlay_color): $bg_return .= ', '; endif;
				$bg_return .= "url('".wp_get_attachment_url($background_image)."')";
			else:
				if ($bg!=false):
					$bg_return = '';
				endif;
			endif;
			
			if ($bg_return!=''):
				$return .= $bg_return . ' !important;';
			endif;
		endif;
		
		$return = str_replace ('overlay:'.$overlay.' !important;','',$return);
		$return = str_replace ('overlay-color:'.$overlay_color.' !important;','',$return);
		$return = str_replace ('gradient-color:'.$gradient_color.' !important;','',$return);
		$return = str_replace ('gradient_direction:'.$gradient_direction.' !important;','',$return);
		$return = str_replace ('gradient_percentage_from:'.$percentage_from.' !important;','',$return);
		$return = str_replace ('gradient_percentage_to:'.$percentage_to.' !important;','',$return);
		
		return $return;
	}
endif;

/* Parses Text Style shortcode attribute of element to Dynamic CSS */
if ( !function_exists( 'gusta_show_dynamic_text_css' ) ):
	function gusta_show_dynamic_text_css ( $atts ) {
		/*
		'el_class' => '',
		'dynamic_css' => '',
		'shatts' => '',
		'el_slug' => '',
		'enable_hover' => '',
		'hover_class' => '',
		'enable_active' => '',
		'active_class' => ''
		*/
		
		extract ($atts);
		
		if (!isset($shatts['tg_'.$el_slug."_tg_normal_tg_text_style"])): $shatts['tg_'.$el_slug."_tg_normal_tg_text_style"]=""; endif;
		if (!isset($dynamic_css[$el_class])): $dynamic_css[$el_class]=""; endif;
		
		$el_text_css = $shatts['tg_'.$el_slug.'_tg_normal_tg_text_style'];

		$mobile = gusta_mobile_css ($el_text_css, $el_class);
		
		$el_text_css = $mobile["new"];
		if ($mobile["media"]!=""):
			$dynamic_css["@media screen and (max-width: 479px)"] = $mobile["media"];
		endif;
				
		if ($el_text_css!=""): ($dynamic_css[$el_class] ? $dynamic_css[$el_class] .= $el_text_css : $dynamic_css[$el_class] = $el_text_css); endif;
		
		if ($enable_hover):
			if (!isset($shatts['tg_'.$el_slug."_tg_hover_tg_text_style"])): $shatts['tg_'.$el_slug."_tg_hover_tg_text_style"]=""; endif;
			$el_text_hover_css = $shatts['tg_'.$el_slug.'_tg_hover_tg_text_style'];
			$el_hover_class = (isset($hover_class) && $hover_class!='' ? $hover_class : $el_class.':hover');
			if (!isset($dynamic_css[$el_hover_class])): $dynamic_css[$el_hover_class]=""; endif;
			if ($el_text_hover_css!=""): ($dynamic_css[$el_hover_class] ? $dynamic_css[$el_hover_class] .= $el_text_hover_css : $dynamic_css[$el_hover_class] = $el_text_hover_css); endif;
		endif;
		
		if ($enable_active):
			if (!isset($shatts['tg_'.$el_slug."_tg_active_tg_text_style"])): $shatts['tg_'.$el_slug."_tg_active_tg_text_style"]=""; endif;
			$el_text_active_css = $shatts['tg_'.$el_slug.'_tg_active_tg_text_style'];
			$el_active_class = (isset($active_class) && $active_class!='' ? $active_class : $el_class.'.active');
			if (!isset($dynamic_css[$el_active_class])): $dynamic_css[$el_active_class]=""; endif;
			if ($el_text_active_css!=""): ($dynamic_css[$el_active_class] ? $dynamic_css[$el_active_class] .= $el_text_active_css : $dynamic_css[$el_active_class] = $el_text_active_css); endif;
		endif;
		
		return $dynamic_css;
	}
endif;


/* Parses Text Size of element on Mobile to Dynamic CSS */
if ( !function_exists( 'gusta_mobile_css' ) ):
	function gusta_mobile_css ($advanced_css, $selector) {
		$mobile_font_size = gusta_get_string_between ($advanced_css, 'mobile-font-size:', ' !important;');
		$mobile_text_align = gusta_get_string_between ($advanced_css, 'mobile-text-align:', ' !important;');
		$css = array();
		$css["new"] = $advanced_css;
		$css["media"] = "";
		if ($mobile_font_size):
			$css["new"] = str_replace ('mobile-font-size:'.$mobile_font_size.' !important;','',$advanced_css);
			$css["media"] = $selector.' { font-size:'.$mobile_font_size.' !important; } ';
		endif;
		if ($mobile_text_align):
			$css["new"] = str_replace ('mobile-text-align:'.$mobile_text_align.' !important;','',$advanced_css);
			$css["media"] = $selector.' { text-align:'.$mobile_text_align.' !important; } ';
		endif;
		return $css;
	}
endif;


/* Parses Icon CSS of element to Dynamic CSS */
if ( !function_exists( 'gusta_show_icon_css' ) ):
	function gusta_show_icon_css ( $atts ) {
		/* 'el_class' => '#'.$vc_id.' i',
		'dynamic_css' => $dynamic_css,
		'shatts' => $atts,
		'el_slug' => 'icon',
		'enable_hover' => 1,
		'hover_class' => '#'.$vc_id.':hover i',
		'enable_active' => 1,
		'active_class' => '#'.$vc_id.'.active i' */
		
		extract ($atts);
		
		$hover_class = ($hover_class!='' ? $hover_class : $el_class.':hover');
		$active_class = (isset($active_class) && $active_class!='' ? $active_class : $el_class.'.active');
		
		$dynamic_css[$el_class] = $dynamic_css[$hover_class] = $dynamic_css[$active_class] = '';
		
		if (isset($shatts[$el_slug.'color'])):
			$dynamic_css[$el_class] .= 'color:'.$shatts[$el_slug.'color'].' !important;';
		endif;
		if ($enable_hover):
			if (isset($shatts[$el_slug.'hovercolor'])):
				$dynamic_css[$hover_class] .= 'color:'.$shatts[$el_slug.'hovercolor'].' !important;';
			endif;
		endif;
		if ($enable_active):
			if (isset($shatts[$el_slug.'activecolor'])):
				$dynamic_css[$active_class] .= 'color:'.$shatts[$el_slug.'activecolor'].' !important;';
			endif;
		endif;
		if (isset($shatts[$el_slug.'background_color'])):
			$dynamic_css[$el_class] .= 'background-color:'.$shatts[$el_slug.'background_color'].' !important;';
		endif;
		if ($enable_hover):
			if (isset($shatts[$el_slug.'hoverbackground_color'])):
				$dynamic_css[$hover_class] .= 'background-color:'.$shatts[$el_slug.'hoverbackground_color'].' !important;';
			endif;
		endif;
		if ($enable_active):
			if (isset($shatts[$el_slug.'activebackground_color'])):
				$dynamic_css[$active_class] .= 'background-color:'.$shatts[$el_slug.'activebackground_color'].' !important;';
			endif;
		endif;
		
		if (isset($shatts[$el_slug.'size'])):
			$dynamic_css[$el_class] .= 'font-size:'.$shatts[$el_slug.'size'].' !important;';
		endif;
		if (isset($shatts[$el_slug.'margin_between'])):
			if ($shatts['icon_position']=='right'): $mar_pos = 'left'; else: $mar_pos = 'right'; endif;
			$dynamic_css[$el_class] .= 'margin-'.$mar_pos.':'.$shatts[$el_slug.'margin_between'].' !important;';
		endif;
		
		return $dynamic_css;
	}
endif;


/* Parses Button CSS of element to Dynamic CSS */
if ( !function_exists( 'gusta_show_button_css' ) ):
	function gusta_show_button_css ( $atts ) {
		
		extract ($atts);
		
		$dynamic_css[$el_class] = $dynamic_css[$el_class.':hover'] = '';

		$dynamic_css = gusta_show_dynamic_text_css ( array (
			'el_class' => $el_class,
			'dynamic_css' => $dynamic_css,
			'shatts' => $shatts,
			'el_slug' => $el_slug,
			'enable_hover' => 1,
			'enable_active' => 0,
			'active_class' => ''
		));

		$dynamic_css = gusta_show_dynamic_css ( array (
			'el_class' => $el_class,
			'dynamic_css' => $dynamic_css,
			'shatts' => $shatts,
			'el_slug' => $el_slug.'_styles',
			'enable_hover' => 1,
			'enable_active' => 0,
			'active_class' => ''
		));

		$dynamic_css = gusta_show_icon_css ( array (
			'el_class' => $el_class.' i',
			'dynamic_css' => $dynamic_css,
			'shatts' => $shatts,
			'el_slug' => $el_slug.'_icon',
			'enable_hover' => 1,
			'hover_class' => $el_class.':hover i',
			'enable_active' => 0,
			'active_class' => ''
		));

		if (isset($shatts[$el_slug.'animatedhover_color']) && $shatts[$el_slug.'animatedhover_color']!=""): 
			$dynamic_css[$el_class.':after'] = 'background: '.$shatts[$el_slug.'animatedhover_color'].' !important;';
		endif;

		if (isset($shatts[$el_slug.'threed_shadow_color']) && $shatts[$el_slug.'threed_shadow_color']!=""): 
			if ($shatts[$el_slug.'threed_shadow']=='brd-top'): $pos = '0 -4px'; $pos_hov = '0 -2px';
			elseif ($shatts[$el_slug.'threed_shadow']=='brd-right'): $pos = '4px 0'; $pos_hov = '2px 0';
			elseif ($shatts[$el_slug.'threed_shadow']=='brd-bottom'): $pos = '0 4px'; $pos_hov = '0 2px';
			elseif ($shatts[$el_slug.'threed_shadow']=='brd-left'): $pos = '-4px 0'; $pos_hov = '-2px 0';
			endif;
			$dynamic_css[$el_class] .= 'box-shadow: '.$pos.' '.$shatts[$el_slug.'threed_shadow_color'].' !important;';
			$dynamic_css[$el_class.':hover'] .= 'box-shadow: '.$pos_hov.' '.$shatts[$el_slug.'threed_shadow_color'].' !important;';
		endif;
		
		return $dynamic_css;
	}
endif;

/* Writes Post Listing Elements Inline Styles */
if(!function_exists('gusta_post_element_style')):
	function gusta_post_element_style ($element, $vc_id, $card_design_class, $dynamic_css, $atts, $add_link=false, $label=true, $text=true) {
		if ($card_design_class!=''):
			if ($add_link=='none' || $element=='container'):
				$box_el_class = '.'.$card_design_class.' .'.$vc_id;
				$box_el_overlay_class = '.'.$card_design_class.' .'.$vc_id.' .gusta-overlay';
				$box_hover_class = '.'.$card_design_class.' .post-listing-container:hover .'.$vc_id;
				$box_hover_overlay_class = '.'.$card_design_class.' .post-listing-container:hover .'.$vc_id.' .gusta-overlay';
			else:
				$box_el_class = '.'.$card_design_class.' .'.$vc_id;
				$box_hover_class = '.'.$card_design_class.' .post-listing-container:hover .'.$vc_id;
				$box_el_overlay_class = '.'.$card_design_class.' .'.$vc_id.' .gusta-overlay';
				$box_hover_overlay_class = '.'.$card_design_class.' .post-listing-container:hover .'.$vc_id.' .gusta-overlay';
			endif;
			
			if ($text):
				$txt_el_class = '.'.$card_design_class.' .'.$vc_id.', .'.$card_design_class.' .'.$vc_id.' a';
				$txt_hover_class = '.'.$card_design_class.' .post-listing-container:hover .'.$vc_id.', .'.$card_design_class.' .post-listing-container:hover .'.$vc_id.' a';
			endif;
			
			if ($label):
				$txt_el_i_class = '.'.$card_design_class.' .'.$vc_id.' i';
				$txt_el_span_class = '.'.$card_design_class.' .'.$vc_id.' span.label-text';
				$txt_hover_i_class = '.'.$card_design_class.' .post-listing-container:hover .'.$vc_id.' i';
				$txt_hover_span_class = '.'.$card_design_class.' .post-listing-container:hover .'.$vc_id.' span.label-text';
			endif;
			
		else:
			if ($add_link=='none'):
				$box_el_class = '.'.$vc_id;
				$box_el_overlay_class = $box_el_class.' .gusta-overlay';
				$box_hover_class = '';
				$box_hover_overlay_class = $box_el_class.':hover .gusta-overlay';
			else:
				$box_el_class = '.'.$vc_id;
				$box_el_overlay_class = '.'.$vc_id.' .gusta-overlay';
				$box_hover_class = '';
				$box_hover_overlay_class = '.'.$vc_id.':hover .gusta-overlay';
			endif;
			
			if ($text):
				$txt_el_class = '.'.$vc_id.', .'.$vc_id.' a';
				$txt_hover_class = '.'.$vc_id.':hover, .'.$vc_id.':hover a';
			endif;
			
			if ($label):
				$txt_el_i_class = '.'.$vc_id.' i';
				$txt_el_span_class = '.'.$vc_id.' span.label-text';
				$txt_hover_i_class = '.'.$vc_id.':hover i';
				$txt_hover_span_class = '.'.$vc_id.':hover span.label-text';
			endif;
		endif;
		
		$dynamic_css = gusta_show_dynamic_css ( array (
			'el_class' => $box_el_class,
			'overlay_class' => $box_el_overlay_class,
			'dynamic_css' => $dynamic_css,
			'shatts' => $atts,
			'el_slug' => $element,
			'enable_hover' => 1,
			'hover_class' => $box_hover_class,
			'hover_overlay_class' => $box_hover_overlay_class,
			'enable_active' => 0,	
		));
		
		if ($text):
			$dynamic_css = gusta_show_dynamic_text_css ( array (
				'el_class' => $txt_el_class,
				'dynamic_css' => $dynamic_css,
				'shatts' => $atts,
				'el_slug' => $element,
				'enable_hover' => 1,
				'hover_class' => $txt_hover_class,
				'enable_active' => 0,
			));
		endif;

		if ($label):
			$dynamic_css = gusta_show_dynamic_css ( array (
				'el_class' => $txt_el_span_class,
				'dynamic_css' => $dynamic_css,
				'shatts' => $atts,
				'el_slug' => 'label_text',
				'enable_hover' => 1,
				'hover_class' => $txt_hover_span_class,
				'enable_active' => 0,
			));
			
			$dynamic_css = gusta_show_dynamic_text_css ( array (
				'el_class' => $txt_el_span_class,
				'dynamic_css' => $dynamic_css,
				'shatts' => $atts,
				'el_slug' => 'label_text',
				'enable_hover' => 1,
				'hover_class' => $txt_hover_span_class,
				'enable_active' => 0,
			));

			$dynamic_css = gusta_show_icon_css ( array (
				'el_class' => $txt_el_i_class,
				'dynamic_css' => $dynamic_css,
				'shatts' => $atts,
				'el_slug' => 'label_icon',
				'enable_hover' => 1,
				'hover_class' => $txt_hover_i_class,
				'enable_active' => 0,
				'active_class' => ''
			));
		endif;
		
		return $dynamic_css;
	}
endif;
?>