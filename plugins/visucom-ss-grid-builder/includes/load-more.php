<?php
/*
* Ajax Load More
*
* @file           includes/load-more.php
* @package        Smart Grid Builder
* @author         Bora Demircan & Ali Metehan Erdem
* @copyright      2018 Theme Gusta
* @license        license.txt
* @version        Release: 1.3.0
*
*/

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/* Load More Posts Query Action */
if ( !function_exists( 'gusta_ajax_load_more' ) ):
	function gusta_ajax_load_more() {
		$query_vars = (isset($_POST['query_vars']) ? unserialize( rawurldecode( base64_decode( strip_tags( $_POST['query_vars'])))) : '');
		$page = (isset($_POST['page']) ? strip_tags($_POST['page']) : 2);
		$items = (isset($_POST['items']) ? strip_tags($_POST['items']) : 10);
		$offset = (isset($_POST['offset']) ? strip_tags($_POST['offset']) : 0);
		$card_design = (isset($_POST['card_design']) ? strip_tags($_POST['card_design']) : 0);
		$query_vars["paged"] = $page;
		$query_vars["offset"] = (( $page - 1 ) * $items ) + $offset;
		
		$tax_ids = $_POST["tax_id"];
		if ($tax_ids):

			$tax_query = array();
			
			$tax_each = explode(',', $tax_ids);
			
			foreach ($tax_each as $tax_id):
				if ($tax_id):
					$tax_ex = explode ('-', $tax_id);
					$taxonomy = $tax_ex[0];
					$taxonomy_id = $tax_ex[1];
		
					if ($taxonomy_id):
			
						$tax_query[] =	array(
							'taxonomy' => $taxonomy,
							'terms'    => $taxonomy_id
						);
						
					endif;
				endif;
			endforeach;
				
			if ($tax_query): 
				if ($query_vars['tax_query']):
					$tax_query = array_merge($query_vars['tax_query'], $tax_query);
				else:
					$tax_query = array_merge(array( 'relation' => 'AND' ), $tax_query);
				endif;
				$query_vars['tax_query'] = $tax_query;
			endif;

		
		endif;
		
		$the_query = new WP_Query( $query_vars );
		
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				global $parent;
				$parent = $the_query->post;
				WPBMap::addAllMappedShortcodes();
				echo '<div class="post-listing-container">'.do_shortcode(get_post_field('post_content', $card_design)).'</div>';
			}
			wp_reset_postdata();
		}
		echo '<div class="gusta-found-posts">'.$the_query->found_posts.'</div>';
		wp_die();
	}
	add_action( 'wp_ajax_gusta_ajax_load_more', 'gusta_ajax_load_more' );
	add_action( 'wp_ajax_nopriv_gusta_ajax_load_more', 'gusta_ajax_load_more' );
endif;
?>