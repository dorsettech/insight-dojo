<?php
/*
* Enqueue Functions
*
* @file           includes/enqueue.php
* @package        Smart Sections
* @author         Bora Demircan & Ali Metehan Erdem
* @copyright      2018 Theme Gusta
* @license        license.txt
* @version        Release: 1.2.9
*
*/

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/* Enqueue Scripts */
if ( !function_exists( 'gusta_enqueue_scripts' ) ):
	function gusta_enqueue_scripts() {
		global $post;
		
		wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js', array( 'jquery' ), true );
		wp_register_script( 'easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.compatibility.min.js', array( 'jquery' ), true);
		wp_register_script( 'loop', SMART_GRID_BUILDER_PLUGIN_URL . 'assets/js/loop.js', array( 'jquery' ), true );
		wp_register_script( 'salvattore', SMART_GRID_BUILDER_PLUGIN_URL . 'assets/js/salvattore.min.js', array( 'jquery' ), true );
		wp_register_script( 'owl-carousel', SMART_GRID_BUILDER_PLUGIN_URL . 'assets/js/owl.carousel.min.js', array( 'jquery' ), true );
		wp_register_script( 'smart-grid-builder', SMART_GRID_BUILDER_PLUGIN_URL . 'assets/js/scripts.js', array( 'jquery' ), true );
		
		//  enqueue the scripts:
		wp_enqueue_script( 'jquery' );
		//wp_enqueue_script( 'easing' );
		
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ):
			wp_enqueue_script( 'comment-reply' );
		endif;
		
		wp_enqueue_script( 'smart-grid-builder' );
		wp_localize_script( 'smart-grid-builder', 'smart_grid_builder', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
		
	}
	add_action( 'wp_footer', 'gusta_enqueue_scripts' );
endif;

/* Enqueue Styles */
if ( !function_exists( 'gusta_enqueue_styles' ) ):
	function gusta_enqueue_styles() {		
		
		// Register the styles for the plugin:
		wp_register_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), '4.7.0', 'all' );
		wp_register_style( 'owl-carousel', SMART_GRID_BUILDER_PLUGIN_URL . '/assets/css/owl.carousel.min.css', array(), '2.3.4', 'all' );
		wp_register_style( 'smart-grid-builder', SMART_GRID_BUILDER_PLUGIN_URL . '/assets/css/style.css', array(), '1.0.1', 'all' );
		
		//  enqueue the styles:
		wp_enqueue_style( 'font-awesome' );
		wp_enqueue_style( 'smart-grid-builder' );
		
	}
	add_action( 'wp_enqueue_scripts', 'gusta_enqueue_styles', 9999 );
endif;


/**************************


ADMIN


***************************/

/* Add Admin Styles and Scripts*/
if ( !function_exists( 'gusta_enqueue_admin_styles' ) ):
	function gusta_enqueue_admin_styles() {
		wp_register_style( 'gusta-admin-styles', SMART_GRID_BUILDER_PLUGIN_URL . 'assets/admin/css/admin_style.css', array(), '1.0.0', 'all' );
		wp_register_style( 'bootstrap-colorpicker', SMART_GRID_BUILDER_PLUGIN_URL . 'assets/admin/css/bootstrap-colorpicker.min.css', array(), '1.0.0', 'all' );
		
		wp_register_script( 'gusta-admin-scripts', SMART_GRID_BUILDER_PLUGIN_URL . 'assets/admin/js/admin_scripts.js', array( 'jquery' ), true );
		wp_register_script( 'bootstrap-colorpicker', SMART_GRID_BUILDER_PLUGIN_URL . 'assets/admin/js/bootstrap-colorpicker.min.js', array( 'jquery' ), true );
		
		wp_enqueue_style( 'bootstrap-colorpicker' );
		wp_enqueue_style( 'gusta-admin-styles' );
		
		wp_enqueue_script( 'bootstrap-colorpicker' );
		wp_enqueue_script( 'gusta-admin-scripts' );
	}
	add_action( 'admin_enqueue_scripts', 'gusta_enqueue_admin_styles' );
endif;