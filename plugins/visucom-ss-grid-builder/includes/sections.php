<?php
/*
* Section Functions
*
* @file           functions/sections.php
* @package        Smart Sections
* @author         Bora Demircan & Ali Metehan Erdem
* @copyright      2017 Theme Gusta
* @license        license.txt
* @version        Release: 1.0.0
*
*/

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/* Outputs an array consisting of the sections filtered by the purpose */
if ( !function_exists( 'gusta_get_sections' ) ):
	function gusta_get_sections ( $type=null, $self=false ) {
		$post_id = '';
		
		$sections_array = array();
		$sections_array[__('Select Section...', 'mb_framework')] = '';
		$args = array( 'post_type' => 'gusta_section', 'posts_per_page' => -1 );
		if ($type!=null):
			$args['meta_query'] = array( array( 'key' => 'gusta_section_purpose', 'value' => $type ) );
		endif;
		if (!$self):
			$url = wp_get_referer();
			$post_id = gusta_get_string_between ($url, 'post=','&');
			
			$post_type = get_post_type($post_id);
			if ($post_type!='gusta_section'): $post_id = ''; endif;
			$args['post__not_in'] = array($post_id);
		endif;
		$sections = new WP_Query( $args );
		foreach ($sections->posts as $section):
			//if (strpos($section->post_content, 'gusta_post_content') === false):
				$sections_array[$section->post_title] = $section->ID;
			//endif;
		endforeach;
		return $sections_array;
	}
endif;
?>