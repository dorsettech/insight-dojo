<?php
/*
* Javascript Functions
*
* @file           functions/js.php
* @package        Smart Sections
* @author         Bora Demircan & Ali Metehan Erdem
* @copyright      2017 Theme Gusta
* @license        license.txt
* @version        Release: 1.0.0
*
*/

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/* Shortcode Dynamic Inline JS to Footer */
if ( !function_exists( 'gusta_inline_shortcode_js' ) ):
	function gusta_inline_shortcode_js ( $dynamic_js, $post_id=null, $type=null ) {
		global $post;
		
		$content = '';
		
		if ($type=='section'):
			$content = gusta_get_section_var ($post_id, 'post_content');
		elseif ($type=='section_vc'):
			$content = get_post_field('post_content', $post_id);
		else:
			if ($post):
				if (is_singular()):
					$content = $post->post_content;
				endif;
			endif;
		endif;
		
		$shortcodes = array (
			'gusta_post_listing',
			'gusta_post_carousel'
		);
		
		if ($content!=''):

			$r = $content;

			foreach ($shortcodes as $shortcode):
				if (strpos($r, "[".$shortcode) !== false):
					
					$sloop=true;
					$i=0;
					while ($sloop==true):
						$atts=array();
						$shatts = gusta_get_string_between($r, '['.$shortcode.' ', ']');
						$atts = shortcode_parse_atts($shatts);
						
						$r = str_replace('['.$shortcode.' '.$shatts.']', '', $r);
						$r = str_replace('['.$shortcode.']', '', $r);
						
						if ($shatts): 
							extract($atts); 
							
							if (isset($vc_id)):
								switch ($shortcode) {
									case 'gusta_post_listing':
										$dynamic_js .= "jQuery('#".$vc_id."').load_more({total_items: jQuery(this).data('total')";
										$dynamic_js .= (isset($items_per_page) && $items_per_page!="" ? ", items_per_page: ".$items_per_page : ", items_per_page: 10");
										$dynamic_js .= (isset($offset) && $offset!="" ? ", offset: ".$offset : "");
										$dynamic_js .= (isset($load_more_style) && $load_more_style!="" ? ", load_more_style: '".$load_more_style."'" : "");
										$dynamic_js .= (isset($load_more_text) && $load_more_text!="" ? ", load_more_text: '".$load_more_text."'" : "");
										$dynamic_js .= "});";
										unset ($post_type, $vc_id, $items_per_page, $offset, $load_more_style, $load_more_text, $columns, $items_layout, $number_of_columns, $number_of_columns_tablet, $number_of_columns_mobile);
									break;
									case 'gusta_taxonomy_listing':
										$dynamic_js .= "jQuery('#".$vc_id."').load_more({total_items: jQuery(this).data('total')";
										$dynamic_js .= (isset($items_per_page) && $items_per_page!="" ? ", items_per_page: ".$items_per_page : ", items_per_page: 10");
										$dynamic_js .= (isset($offset) && $offset!="" ? ", offset: ".$offset : "");
										$dynamic_js .= "});";
										unset ($taxonomy, $vc_id, $items_per_page, $offset, $columns, $items_layout, $number_of_columns, $number_of_columns_tablet, $number_of_columns_mobile);
									break;
									case 'gusta_post_carousel':
										$loop = (isset($loop) && $loop!="" ? $loop : 'false');
										$dynamic_js .= "jQuery('#".$vc_id." .owl-carousel').owlCarousel({autoHeight:true, navText: [jQuery('.".$vc_id." .gusta-prev'),jQuery('.".$vc_id." .gusta-next')], loop: ".$loop."";
										$number_of_columns = (isset($number_of_columns) && $number_of_columns!="" ? $number_of_columns : '1');
										$number_of_columns_tablet = (isset($number_of_columns_tablet) && $number_of_columns_tablet!="" ? $number_of_columns_tablet : '1');
										$number_of_columns_mobile = (isset($number_of_columns_mobile) && $number_of_columns_mobile!="" ? $number_of_columns_mobile : '1');
										$stage_padding = (isset($stage_padding) && $stage_padding!="" ? $stage_padding : '0');
										$stage_padding_tablet = (isset($stage_padding_tablet) && $stage_padding_tablet!="" ? $stage_padding_tablet : '0');
										$stage_padding_mobile = (isset($stage_padding_mobile) && $stage_padding_mobile!="" ? $stage_padding_mobile : '0');
										$dynamic_js .= ", responsiveClass:true, responsive:{0:{items:".$number_of_columns_mobile.",stagePadding: ".$stage_padding_mobile."},768:{items:".$number_of_columns_tablet.",stagePadding: ".$stage_padding_tablet."},1024:{
            items:".$number_of_columns.",stagePadding: ".$stage_padding."}}";
										$dynamic_js .= (isset($navigation) && $navigation!="" ? ", nav: ".$navigation : "");
										$dynamic_js .= (isset($dots) && $dots!="" ? ", dots: ".$dots : "");
										$dynamic_js .= (isset($gap) && $gap!="" ? ", margin: ".$gap : ", margin: 30");
										$dynamic_js .= (isset($autoplay) && $autoplay!="" ? ", autoplay: ".$autoplay.", autoplayHoverPause:true" : "");
										if (isset($autoplay) && $autoplay!=""):
											$dynamic_js .= (isset($autoplay_timeout) && $autoplay_timeout!="" ? ", autoplayTimeout: '".$autoplay_timeout."'" : ", autoplayTimeout:5000");
										endif;
										$dynamic_js .= "});";
										unset ($post_type, $vc_id, $loop, $navigation, $dots, $gap, $autoplay, $autoplay_timeout, $number_of_columns, $number_of_columns_tablet, $number_of_columns_mobile, $stage_padding, $stage_padding_tablet, $stage_padding_mobile);
									break;
								}
							endif;
						unset ($atts);
						endif;
						
						if (strpos($r, "[".$shortcode) === false):
							$sloop=false;
						endif;
						$i++;
						if ($i==50) : $loop=false; endif;
					
					endwhile;
				endif;
			endforeach;
		endif;
		
		return $dynamic_js;
	}
endif;


/* Convert Columns to Bootstrap class number */
if ( !function_exists( 'gusta_columns' ) ):
	function gusta_columns ($from) {
		if ($from=='12'): $ret = '1';
		elseif ($from=='6'): $ret = '2';
		elseif ($from=='4'): $ret = '3';
		elseif ($from=='3'): $ret = '4';
		elseif ($from=='5-12'): $ret = '5';
		else: $ret = '6';
		endif;
		return $ret;
	}
endif;

/* This function parses all the inline javascript of shortcode inside content of the page and the included sections. */
if ( !function_exists( 'gusta_parse_dynamic_js' ) ):
	function gusta_parse_dynamic_js () {
		$dynamic_js = $sticky_js = "";
		
		$sections_js = "jQuery(document).on('ready',function(){ 'use strict'; ";
		
		$dynamic_js = gusta_inline_shortcode_js( $dynamic_js, get_the_id() );
		
		$dynamic_js .= $sections_js . "});"; 
		
		if ($dynamic_js!=""):
			echo '
	<script type="text/javascript" id="gusta_dynamic_js">'.$dynamic_js.'</script>
	';
		endif;
	}
	add_action ( 'wp_footer', 'gusta_parse_dynamic_js', 999 );
endif;
?>