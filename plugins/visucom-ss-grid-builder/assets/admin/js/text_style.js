!function($) {
	jQuery('.gusta-color-picker').colorpicker({format: null});
	
	jQuery('.text_style').bind("keyup input change", function() {
		
		var newtext = '';
		var parent = jQuery(this).data('parent');
		jQuery('.text_style').each(function(){
			if (jQuery(this).data('parent')===parent) {
				var str = jQuery(this).val();
				if (str!='') {
					if(jQuery(this).attr('type') == 'radio') {
						if(jQuery(this).is(':checked')) {
							newtext = newtext + jQuery(this).data('attr') + ':' + str + ' !important;';
						}
					} else  {
						newtext = newtext + jQuery(this).data('attr') + ':' + str + ' !important;';
					}
				}
			}
		});
		jQuery('input.' + parent).val(newtext);
	});
	
}(window.jQuery);