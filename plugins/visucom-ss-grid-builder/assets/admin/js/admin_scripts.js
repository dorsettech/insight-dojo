jQuery(function($){ 

	jQuery(".smooth-scroll, .smooth-scroll a").click(function(event){ 
		event.preventDefault(); 
		var dest=0; 
		if(jQuery(this.hash).offset().top > jQuery(document).height()-jQuery(window).height()){ 
			dest=jQuery(document).height()-jQuery(window).height(); 
		} else { 
			dest=jQuery(this.hash).offset().top; 
		} 
		jQuery('html,body').animate({scrollTop:dest}, 1000,'swing'); 
	});
	
	jQuery('div[data-name*="_sections_tab"]').find('label[for*="_sections_tab"]').click(function() {
		jQuery(this).parent().parent().toggleClass('gusta_tab_active');
		jQuery(this).parent().parent().find('.acf-fields').slideToggle();
	});
  
});