<?php
/*
* Plugin Name: Smart Grid Builder
* Plugin URI: https://themegusta.com/smart-grid-builder
* Description: Create any kind of grid layout easily and visually.
* Author: Theme Gusta
* Text Domain: mb_framework
* Domain Path: /languages
* Version: 1.3.0
* Author URI: https://themegusta.com
*/

if (!defined('SMART_SECTIONS_PLUGIN_PATH')):
  define( 'SMART_GRID_BUILDER_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
  define( 'SMART_GRID_BUILDER_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

  if(!function_exists('gusta_textdomain')):
  	function gusta_textdomain() {
  	  load_theme_textdomain( 'mb_framework', plugin_dir_path( __FILE__ ) . 'languages' );
  	}
  	add_action( 'after_setup_theme', 'gusta_textdomain' );
  endif;

  if ( defined( 'WPB_VC_VERSION' ) ):

    //Outputs the post type of the archive type of the page being viewed
    if(!function_exists('gusta_get_post_type')):
      function gusta_get_post_type () {
        global $wp;
        if (defined('GUSTA_POST_TYPE')): return GUSTA_POST_TYPE; endif;
        $current_path = $_SERVER['PHP_SELF'];
        if (isset($_GET["taxonomy"])):
          $post_type = $_GET["taxonomy"];
          if (($post_type!='category') && ($post_type!='post_tag')): $post_type = 'tax'; endif;
        elseif (isset($_GET["post"])):
          $post_type = get_post_type($_GET["post"]);
          if (($post_type!='page') && ($post_type!='post') && ($post_type!='gusta_section')): $post_type = 'cpt'; endif;
        elseif (strpos($current_path, 'post-new.php') !== false):
          $post_type = (isset($_GET["post_type"]) ? $_GET["post_type"] : 'post');
          if (($post_type!='page') && ($post_type!='post') && ($post_type!='gusta_section')): $post_type = 'cpt'; endif;
        elseif (strpos($current_path, 'profile.php') !== false):
          $post_type = 'author';
        else:
          $post_type = '';
        endif;
        define ('GUSTA_POST_TYPE', $post_type);
        return $post_type;
      }
      $post_type = gusta_get_post_type();
    endif;
    
    include( SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/php.php');
    include( SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/wp.php');
    include( SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/enqueue.php');
    include( SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/vc.php');
    include( SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/register.php');
    include( SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/sections.php');
    include( SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/css.php');
    include( SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/excerpt.php');
    include( SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/image.php');
    include( SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/social.php');
    include( SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/js.php');
    include( SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/load-more.php');
    include( SMART_GRID_BUILDER_PLUGIN_PATH . 'one-click-demo-import/one-click-demo-import.php');
    //include( SMART_GRID_BUILDER_PLUGIN_PATH . 'includes/demo-importer/gusta-demo.php');
    
    if(!function_exists('gusta_set_default_meta_card')):
		function gusta_set_default_meta_card($post_ID){
		    $current_field_value = get_post_meta($post_ID,'gusta_section_purpose',true);
		    $default_meta = 'card'; // value
		    if ($current_field_value == '' && !wp_is_post_revision($post_ID)){
		            add_post_meta($post_ID,'gusta_section_purpose',$default_meta,true);
		    }
		    return $post_ID;
		}
		add_action('wp_insert_post','gusta_set_default_meta_card');
	endif;
    
    if(!function_exists('gusta_activation')):
      function gusta_activation() {
        add_option( 'gusta_extraction_length', '255', '', 'yes' );
      }
      register_activation_hook( __FILE__, 'gusta_activation' );
    endif;
    
    if(!function_exists('gusta_shortcode_mappers')):
      function gusta_shortcode_mappers() {
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_social_sharing_box.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_listing.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_filter.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_carousel.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_taxonomy_listing.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_title.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_content.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_excerpt.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_featured_image.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_featured_image_as_background.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_date.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_author.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_author_image.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_categories.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_tags.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_taxonomies.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_read_more_button.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_post_custom_field.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_text.php' );
        include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_container.php' );
        if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	      	include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_add_to_cart.php' );
	      	include( SMART_GRID_BUILDER_PLUGIN_PATH . 'vc_elements/gusta_product_price.php' );
	}
      }
      add_action( 'init', 'gusta_shortcode_mappers', 9999 );
    endif;
	
	// Check the current theme slug
	if(!function_exists('gusta_theme_name')):
		function gusta_theme_name() {
			$theme_name = str_replace( get_bloginfo( 'url' ).'/wp-content/themes/', '', get_bloginfo( 'stylesheet_directory' ));
			return $theme_name;
		}
	endif;

	//Adds a script at the head sections that makes the rows full width 
  if(!function_exists('gusta_header_buffer_holder')):
    function gusta_header_buffer_holder(){
      ob_start();

      if (in_array(gusta_theme_name(), array('bridge','jupiter'))):
        if (in_array(gusta_theme_name(), array('bridge'))): 
          $width = 'outerWidth()+30'; 
        else:
          $width = 'outerWidth()'; 
        endif;
        echo "<script>function gusta_fix_vc_full_width() {var fix = 1, jQueryelement=jQuery('.vc_row-o-full-height:first'); if(jQueryelement.length){ jQueryelement.css('min-height','100vh'); } jQuery(document).trigger('vc-full-height-row',jQueryelement); jQuery.each(jQuery('[data-vc-full-width=\"true\"]:not(.vc_clearfix, [data-vc-stretch-content=\"true\"]), .vc_row-full-width:not(.vc_clearfix, [data-vc-stretch-content=\"true\"])'),function(){ var el=jQuery(this); el.css({'left':'','width':'','box-sizing':'','max-width':'','padding-left':'','padding-right':''}); var el_width = el.parent().".$width.", width = jQuery(window).width() + 2; el_margin = parseInt((width - el_width) / 2 + 1); if (el_margin!='0' && fix==1) { el.addClass('vc_hidden').css({'left':-el_margin, 'width':width, 'box-sizing':'border-box', 'max-width':width, 'padding-left':el_margin, 'padding-right':el_margin }).attr('data-vc-full-width-init','true').removeClass('vc_hidden');}}); fix = 0; jQuery('.owl-carousel').each(function(){ jQuery(this).trigger('refresh.owl.carousel'); }); } jQuery( 'document' ).ready(function() { gusta_fix_vc_full_width(); });</script>";
      else:
        echo "<script>function gusta_fix_vc_full_width() { var elements=jQuery('[data-vc-full-width=\"true\"]');jQuery.each(elements,function(key,item){var el=jQuery(this);el.addClass('vc_hidden');var el_full=el.next('.vc_row-full-width');if(el_full.length||(el_full=el.parent().next('.vc_row-full-width')),el_full.length){var el_margin_left=parseInt(el.css('margin-left'),10),el_margin_right=parseInt(el.css('margin-right'),10),offset=0-el_full.offset().left-el_margin_left,width=jQuery(window).width();if(el.css({position:'relative',left:offset,'box-sizing':'border-box',width:jQuery(window).width()}),!el.data('vcStretchContent')){var padding=-1*offset;0>padding&&(padding=0);var paddingRight=width-padding-el_full.width()+el_margin_left+el_margin_right;0>paddingRight&&(paddingRight=0),el.css({'padding-left':padding+'px','padding-right':paddingRight+'px'})}el.attr('data-vc-full-width-init','true'),el.removeClass('vc_hidden'),jQuery(document).trigger('vc-full-width-row-single',{el:el,offset:offset,marginLeft:el_margin_left,marginRight:el_margin_right,elFull:el_full,width:width})}}),jQuery(document).trigger('vc-full-width-row',elements); jQuery('.owl-carousel').each(function(){ jQuery(this).trigger('refresh.owl.carousel'); }); } jQuery( 'document' ).ready(function() { gusta_fix_vc_full_width(); });</script>";
      endif;    
    }
	if (!is_admin()):
    	add_action( 'wp_head', 'gusta_header_buffer_holder');
	endif;
  endif;

  else:
    
    if(!function_exists('gusta_admin_notice_warning')):
      //Visual Composer requirement notice
      function gusta_admin_notice_warning() {
        $class = 'notice notice-warning';
        $message = __( 'The Smart Grid Builder plugin only works if the WPBakery Page Builder plugin is installed and activated.', 'mb_framework' );

        printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
      }
      add_action( 'admin_notices', 'gusta_admin_notice_warning' );
    endif;
    
  endif;
endif;
?>