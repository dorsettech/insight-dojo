<?php
/*
* Visual Composer Add to Cart Element & Shortcode
*
* @file           vc_elements/gusta_add_to_cart.php
* @package        Smart Grid Builder
* @author         Bora Demircan & Ali Metehan Erdem
* @copyright      2018 Theme Gusta
* @license        license.txt
* @version        Release: 1.1.9
*
*/

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/*
Element Description: Gusta Add to Cart
*/
 
// Element HTML
    function gusta_add_to_cart_html( $atts ) {
		global $parent, $product;
		$the_post = $parent;
		if (!$product): $product = wc_get_product($the_post->ID); endif;
		if ($product): $the_post=get_post($product->get_id()); endif;
		if ($the_post==''): $the_post=get_queried_object(); endif;
        $css = $el_class = $output = $the_permalink = $the_date = ''; unset ($dynamic_css);
		
		$att = shortcode_atts(array(
			'vc_id' => '',
			'element_tag' => 'p',
			'alignment' => 'left',
			'display_inline' => '',
			'mobile_display' => '',
			'mobile_alignment' => '',
			'mobile_display_inline' => '',
			'visibility' => 'show-show',
			'animation' => 'fade',
			'el_class' => '',
		), $atts, 'gusta_add_to_cart');
		extract($att);
        
		$post_id='';
		if ($the_post):
			$post_id = $the_post->ID;
		endif;
		
		if ($post_id):
			$link_class='';

			$mobile_disp = gusta_mobile_display($att);
			
			$output = '<div class="gusta-add-to-cart gusta-align-'.$alignment.' '.$display_inline.$mobile_disp.'"><'.$element_tag.' class="'.$vc_id.' ss-element gusta-add-to-cart '.$visibility.' '.$el_class.'"><a href="'.site_url().'/?add-to-cart='.$post_id.'" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="'.$post_id.'" aria-label="'.__('Add to Cart', 'mb_framework').'" rel="nofollow">'.__('Add to Cart', 'mb_framework').'</a></'.$element_tag.'></div>';
			
			$output .= gusta_clear($att);
			
			return $output;
		endif;
        
    }

    add_shortcode( 'gusta_add_to_cart', 'gusta_add_to_cart_html' );
	


    // Element Mapping
		
		$params = array (
			gusta_vc_id('add-to-cart')
		);

		$params = gusta_element_display($params);
		$params = gusta_visibility_hover_animation($params);
		$params[] = gusta_vc_extra_class_name();

		$params = gusta_styles_tab ( $params, array ( 
			array (	'sub_group' => __( 'Button', 'mb_framework' ), 'el_slug' => 'button', 'dependency' => 0, 'enable_hover' => 1, 'enable_active' => 0, 'enable_box' => 1, 'enable_text' => 1 ),
		));

		// Map the block with vc_map()
		vc_map( 
			array(
				"name" => __("Add to Cart Button", "mb_framework"), // add a name
				"base" => "gusta_add_to_cart", // bind with our shortcode
				"content_element" => true, // set this parameter when element will has a content
				"is_container" => false, // set this param when you need to add a content element in this element
				'admin_enqueue_css' => array( SMART_GRID_BUILDER_PLUGIN_URL . '/assets/admin/css/vc_style.css' ),
				"category" => __('Smart Grid Builder', 'mb_framework'),
				"params" => $params
			)
		);
     
     unset($params);