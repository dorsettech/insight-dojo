<?php
/*
* Visual Composer Post Filter Element & Shortcode
*
* @file           vc_elements/gusta_post_filter.php
* @package        Smart Grid Builder
* @author         Bora Demircan & Ali Metehan Erdem
* @copyright      2018 Theme Gusta
* @license        license.txt
* @version        Release: 1.2.5
*
*/

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/*
Element Description: Gusta Post Filter
*/

// Element HTML
    function gusta_post_filter_html( $atts ) {

        $css = $el_class = $output = ''; unset ($dynamic_css);
		
		$fields = array(
			'vc_id' => '',
			'filters' => '',
			'post_listing_id' => '',
			'alignment' => 'left',
			'display_inline' => '',
			'mobile_display' => '',
			'mobile_alignment' => '',
			'mobile_display_inline' => '',
			'el_class' => '',
		);
		
		$att = shortcode_atts($fields, $atts, 'gusta_post_listing_filter');
		extract($att);
		
		$mobile_disp = gusta_mobile_display($att);
		
		$filters = (array) vc_param_group_parse_atts( $filters );
		
		$output .= '<div class="gusta-align-'.$alignment.' '.$display_inline.$mobile_disp.'"><div id="'.$vc_id.'" class="ss-element gusta-post-filter '.$el_class.'" data-post-listing="'.$post_listing_id.'">';
		
		foreach ($filters as $filter):
			$taxonomy = $filter["taxonomy"];
			$type = $filter["type"];
			$all_text = $filter["all_text"];
			$hide_empty = (isset($filter["hide_empty"]) ? $filter["hide_empty"] : '');
			
			if ($hide_empty == 'true'): $hide_empty = true; else: $hide_empty = false; endif;
			
			$terms = get_terms( array(
			    'taxonomy' => $taxonomy,
			    'hide_empty' => $hide_empty,
			) );
			if ($type=='dropdown'):
				$output .= '<div class="gusta-type-dropdown-container"><button class="gusta-type-button" type="button">'.($all_text!='' ? $all_text : __('All','mb_framework')).'<i class="fa fa-sort-down"></i></button> <ul id="filter-'.$taxonomy.'" class="gusta-type-dropdown">
				<li class="gusta-active" id="'.$taxonomy.'-0"><a>'.($all_text!='' ? $all_text : __('All','mb_framework')).'</a></li>';
				foreach($terms as $term):
					$output .= '<li id="'.$taxonomy.'-'.$term->term_id.'"><a>'.$term->name.'</a></li>';
				endforeach;
				$output .= '</ul></div>';
			else:
				$output .= '<ul id="filter-'.$taxonomy.'" class="gusta-type-tabs">
				<li class="gusta-active" id="'.$taxonomy.'-0"><a>'.($all_text!='' ? $all_text : __('All','mb_framework')).'</a></li>';
				foreach($terms as $term):
					$output .= '<li id="'.$taxonomy.'-'.$term->term_id.'"><a>'.$term->name.'</a></li>';
				endforeach;
				$output .= '</ul>';
			endif;
					
		endforeach;
		
		$output .= '</div></div>';
		
		$output .= gusta_clear($att);
				
		return $output;
        
    }
    add_shortcode( 'gusta_post_filter', 'gusta_post_filter_html' );
     
    // Element Mapping
        global $post;
		
	$args = array(
	  'public'   => true
	  
	); 
	$output = 'names'; // or objects
	$operator = 'and'; // 'and' or 'or'
	$taxonomies = get_taxonomies( $args, $output, $operator ); 	
		
     	$params = array (
			array(
				'type' => 'param_group',
				'heading' => __( 'Filters', 'mb_framework' ),
				'param_name' => 'filters',
				'value' => /*urlencode( json_encode( array(
					array(
						'text' => __( 'Lorem ipsum dolor sit amet', 'mb_framework' ),
						'link' => '',
					),
					array(
						'text' => __( 'Sed ut perspiciatis unde omnis iste natus', 'mb_framework' ),
						'link' => '',
					),
				) ) )*/'',
				'params' => array(
					array(
						'type' => 'dropdown',
						'heading' => __( 'Taxonomy', 'js_composer' ),
						'param_name' => 'taxonomy',
						'description' => __( 'Select the taxonomy for filter.', 'mb_framework' ),
						'value' => $taxonomies,
						'admin_label' => true,
					),
					array(
						'type' => 'dropdown',
						'heading' => __( 'Type', 'js_composer' ),
						'param_name' => 'type',
						'description' => __( 'Select the type of the filter (tabs or dropdown).', 'mb_framework' ),
						'value' => array (
							__('Tabs', 'mb_framework') => 'tabs',
							__('Dropdown', 'mb_framework') => 'dropdown' 
						),
						'admin_label' => false,
						'std' => 'tabs',
					),
					array(
						'type' => 'checkbox',
						'heading' => __( 'Hide Empty', 'js_composer' ),
						'param_name' => 'hide_empty',
						'description' => __( 'Hide the taxonomies which have no posts.', 'mb_framework' ),
						'value' => array (
							__('Yes', 'mb_framework') => 'true',
						),
						'admin_label' => false,
						'std' => '',
					),
					array(
						'type' => 'textfield',
						'heading' => __( '"All" Text', 'mb_framework' ),
						'param_name' => 'all_text',
						'description' => __( 'Enter the text for the "All" tabs in the filter (default: All).', 'mb_framework' ),
						'admin_label' => false,
						'std' => __('All', 'mb_framework'),
					),
				),
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Post Listing ID', 'js_composer' ),
				'param_name' => 'post_listing_id',
				'description' => __( 'Enter the post listing unique ID (i.e. post-listing-4388115195bc1f840c1de3). Leave empty if you have only one post listing on your page.', 'mb_framework' ),
				'admin_label' => false,
				'std' => '',
			),
			gusta_vc_id('post-filter'),
			gusta_vc_extra_class_name(),
		);
		
		$params = gusta_element_display($params);


		$params = gusta_styles_tab ( $params, array ( 
			array (	'sub_group' => __( 'Dropdown Button', 'mb_framework' ), 'el_slug' => 'dropdown_button', 'enable_hover' => 1, 'enable_active' => 0, 'enable_box' => 1, 'enable_text' => 1),
			array (	'sub_group' => __( 'Filter Items', 'mb_framework' ), 'el_slug' => 'filter_items', 'enable_hover' => 1, 'enable_active' => 1, 'enable_box' => 1, 'enable_text' => 1),
			array (	'sub_group' => __( 'Filter Container', 'mb_framework' ), 'el_slug' => 'container', 'enable_hover' => 1, 'enable_active' => 0, 'enable_box' => 1, 'enable_text' => 0),
		));

		// Map the block with vc_map()
		vc_map( 
			array(
				"name" => __("Post Listing Filter", "mb_framework"), // add a name
				"base" => "gusta_post_filter", // bind with our shortcode
				"content_element" => true, // set this parameter when element will has a content
				"is_container" => false, // set this param when you need to add a content element in this element
				'admin_enqueue_css' => array( SMART_GRID_BUILDER_PLUGIN_URL . '/assets/admin/css/vc_style.css' ),
				"category" => __('Smart Grid Builder', 'mb_framework'),
				"params" => $params
			)
		);
		unset($params);