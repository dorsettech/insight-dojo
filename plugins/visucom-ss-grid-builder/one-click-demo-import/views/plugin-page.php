<?php
/**
 * The plugin page view - the "settings" page of the plugin.
 *
 * @package ocdi
 */

namespace OCDI;

$predefined_themes = $this->import_files;

if ( ! empty( $this->import_files ) && isset( $_GET['import-mode'] ) && 'manual' === $_GET['import-mode'] ) {
	$predefined_themes = array();
}

?>

<div class="ocdi  wrap  about-wrap">

	<?php ob_start(); ?>
		<h1 class="ocdi__title"><?php esc_html_e( 'Demo Importer', 'mb_framework' ); ?></h1>
	<?php
	$plugin_title = ob_get_clean();

	// Display the plugin title (can be replaced with custom title text through the filter below).
	echo wp_kses_post( apply_filters( 'pt-ocdi/plugin_page_title', $plugin_title ) );

	// Display warrning if PHP safe mode is enabled, since we wont be able to change the max_execution_time.
	if ( ini_get( 'safe_mode' ) ) {
		printf(
			esc_html__( '%sWarning: your server is using %sPHP safe mode%s. This means that you might experience server timeout errors.%s', 'mb_framework' ),
			'<div class="notice  notice-warning  is-dismissible"><p>',
			'<strong>',
			'</strong>',
			'</p></div>'
		);
	}

	// Start output buffer for displaying the plugin intro text.
	ob_start();
	?>

	<div class="ocdi__intro-notice  notice  notice-warning  is-dismissible">
		<p><?php esc_html_e( 'Before you begin, make sure all the required plugins are activated.', 'mb_framework' ); ?></p>
	</div>

	 <div class="ocdi__intro-text">
		<?php /*<p class="about-description">
			<?php esc_html_e( 'Quickly import Smart Section Theme Builder demo sections. This will provide you with a basic layout to build your website and speed up the development process.', 'mb_framework' ); ?>
		</p>

		<p><strong><?php esc_html_e( 'NOTE: Your existing content will NOT be deleted or modified.', 'mb_framework' ); ?></strong></p>

		<?php if ( ! empty( $this->import_files ) ) : ?>
			<?php if ( empty( $_GET['import-mode'] ) || 'manual' !== $_GET['import-mode'] ) : ?>
				<a href="<?php echo esc_url( add_query_arg( array( 'page' => $this->plugin_page_setup['menu_slug'], 'import-mode' => 'manual' ), admin_url( $this->plugin_page_setup['parent_slug'] ) ) ); ?>" class="ocdi__import-mode-switch"><?php esc_html_e( 'Switch to manual import!', 'mb_framework' ); ?></a>
			<?php else : ?>
				<a href="<?php echo esc_url( add_query_arg( array( 'page' => $this->plugin_page_setup['menu_slug'] ), admin_url( $this->plugin_page_setup['parent_slug'] ) ) ); ?>" class="ocdi__import-mode-switch"><?php esc_html_e( 'Switch back to theme predefined imports!', 'mb_framework' ); ?></a>
			<?php endif; ?>
		<?php endif; ?>

		<hr>*/ ?>
	</div>

	<?php
	$plugin_intro_text = ob_get_clean();

	// Display the plugin intro text (can be replaced with custom text through the filter below).
	echo wp_kses_post( apply_filters( 'pt-ocdi/plugin_intro_text', $plugin_intro_text ) );
	?>

	<?php if ( empty( $this->import_files ) ) : ?>
		<div class="notice  notice-info  is-dismissible">
			<p><?php esc_html_e( 'Seems like there is a problem with gthe theme importer. Please upload the import files manually!', 'mb_framework' ); ?></p>
		</div>
	<?php endif;
	$code = (isset($_POST["gusta-purchase-code"]) ? strip_tags($_POST["gusta-purchase-code"]) : get_option('smart_grid_purchase_code'));
	if (isset($_POST["gusta-new-purchase-code"])):
		update_option( 'smart_grid_purchase_code', '' );
		echo '<script>location.reload();</script>';
	endif;
	if (isset($_POST["gusta-verify-purchase-code"])):
		if ($code!=''):
			update_option( 'smart_grid_purchase_code', $code );
			echo '<script>location.reload();</script>';
		endif;
	endif;
	?>
	<div class="gusta-demo-wrap">
		<form method="post" id="purchase-code-status">
			<div class="section">
				<h3>Verify your Envato Purchase</h3>
				<p>In order to be able to import the demos listed below, you need to verify your Envato purchase. <a target="_blank" href="https://help.market.envato.com/hc/en-us/articles/202822600-Where-Is-My-Purchase-Code-">Where is my purchase code?</a></p>
				
				
	<?php if (!gusta_envato_purchase_validation ($code)): ?>
		<div class="verified-status">
			<p>
				<span><strong>Verified:</strong> <span class="echo-purchase-code"><?php echo $code; ?></span></span>
				<span>— <input type="submit" name="gusta-new-purchase-code" onclick="return confirm('Your current purchase code will be deleted to enter new one. Are you sure?')" value="Enter a new purchase code" /></span>
			</p>
		</div>
	
	
	
	<?php else: ?>
				
				<div class="input-hold">
					<input type="text" name="gusta-purchase-code" value="<?php echo $code; ?>">
					<input type="submit" value="Verify" name="gusta-verify-purchase-code" data-loading="Verifying.." class="button button-primary">
					<?php if ($code!=''): ?><p class="gusta-purchase-code-error">
						Your purchase code is invalid or expired.
					</p><?php endif; ?>
					<p class="gusta-import-notice">Please be aware that images used in the demos are not included in the imports. We have used the following Google Fonts in our demos: Roboto, Playfair Display, Poppins, Nunito and Open Sans. If you do not add these fonts from the <a target="_blank" href="https://www.youtube.com/watch?v=iAZ5rOJz1ek&amp;index=4&amp;list=PLhHXQ5n2SiyHgqG7qr-DMLKkJSu3N-7fO">Font Manager</a>, texts will be displayed with your default theme fonts. Modified versions of WPBakery Page Builder bundled with several themes may cause some minor problems with Smart Sections. Since these demos were created using native Visual Composer shortcodes and architecture, it may conflict with your theme shortcodes or architecture. If you do not get the desired result after importing the demos, that might mean that the demos are not compatible with the modified WPBakery Page Builder of your theme, please try creating your own sections with the modified version of WPBakery Page Builder in your theme. If you feel you have a major issue, please open a ticket to let us know. <a target="_blank" href="https://support.themegusta.com">Theme Gusta Support</a></p>
				</div>

			
	<?php endif; ?>
	
	</div>
		</form>
	</div>
	
	
	<?php 
	if (!gusta_envato_purchase_validation ($code)): ?>
	<h3>Demos</h3>
	<?php if ( empty( $predefined_themes ) ) : ?>

	<div class="ocdi__file-upload-container">
		<h2><?php esc_html_e( 'Manual demo files upload', 'mb_framework' ); ?></h2>

		<div class="ocdi__file-upload">
			<h3><label for="content-file-upload"><?php esc_html_e( 'Choose a XML file for content import:', 'mb_framework' ); ?></label></h3>
			<input id="ocdi__content-file-upload" type="file" name="content-file-upload">
		</div>

		<div class="ocdi__file-upload">
			<h3><label for="widget-file-upload"><?php esc_html_e( 'Choose a WIE or JSON file for widget import:', 'mb_framework' ); ?></label></h3>
			<input id="ocdi__widget-file-upload" type="file" name="widget-file-upload">
		</div>

		<div class="ocdi__file-upload">
			<h3><label for="customizer-file-upload"><?php esc_html_e( 'Choose a DAT file for customizer import:', 'mb_framework' ); ?></label></h3>
			<input id="ocdi__customizer-file-upload" type="file" name="customizer-file-upload">
		</div>

		<?php if ( class_exists( 'ReduxFramework' ) ) : ?>
		<div class="ocdi__file-upload">
			<h3><label for="redux-file-upload"><?php esc_html_e( 'Choose a JSON file for Redux import:', 'mb_framework' ); ?></label></h3>
			<input id="ocdi__redux-file-upload" type="file" name="redux-file-upload">
			<div>
				<label for="redux-option-name" class="ocdi__redux-option-name-label"><?php esc_html_e( 'Enter the Redux option name:', 'mb_framework' ); ?></label>
				<input id="ocdi__redux-option-name" type="text" name="redux-option-name">
			</div>
		</div>
		<?php endif; ?>
	</div>

	<p class="ocdi__button-container">
		<button class="ocdi__button  button  button-hero  button-primary  js-ocdi-import-data"><?php esc_html_e( 'Import Demo Data', 'mb_framework' ); ?></button>
	</p>

	<?php elseif ( 1 === count( $predefined_themes ) ) : ?>

	<div class="ocdi__demo-import-notice  js-ocdi-demo-import-notice"><?php
		if ( is_array( $predefined_themes ) && ! empty( $predefined_themes[0]['import_notice'] ) ) {
			echo wp_kses_post( $predefined_themes[0]['import_notice'] );
		}
		?></div>

	<p class="ocdi__button-container">
		<button class="ocdi__button  button  button-hero  button-primary  js-ocdi-import-data"><?php esc_html_e( 'Import Demo Data', 'mb_framework' ); ?></button>
	</p>

	<?php else : ?>

	<!-- OCDI grid layout -->
	<div class="ocdi__gl  js-ocdi-gl">
		<?php
		// Prepare navigation data.
		$categories = Helpers::get_all_demo_import_categories( $predefined_themes );
		?>
		<?php if ( ! empty( $categories ) ) : ?>
		<div class="ocdi__gl-header  js-ocdi-gl-header">
			<nav class="ocdi__gl-navigation">
				<ul>
					<li class="active"><a href="#all" class="ocdi__gl-navigation-link  js-ocdi-nav-link"><?php esc_html_e( 'All', 'mb_framework' ); ?></a></li>
					<?php foreach ( $categories as $key => $name ) : ?>
					<li><a href="#<?php echo esc_attr( $key ); ?>" class="ocdi__gl-navigation-link  js-ocdi-nav-link"><?php echo esc_html( $name ); ?></a></li>
					<?php endforeach; ?>
				</ul>
			</nav>
			<div clas="ocdi__gl-search">
				<input type="search" class="ocdi__gl-search-input  js-ocdi-gl-search" name="ocdi-gl-search" value="" placeholder="<?php esc_html_e( 'Search demos...', 'mb_framework' ); ?>">
			</div>
		</div>
		<?php endif; ?>
		<div class="ocdi__gl-item-container  wp-clearfix  js-ocdi-gl-item-container">
			<?php foreach ( $predefined_themes as $index => $import_file ) : ?>
			<?php
			// Prepare import item display data.
			$img_src = isset( $import_file['import_preview_image_url'] ) ? $import_file['import_preview_image_url'] : '';
			// Default to the theme screenshot, if a custom preview image is not defined.
			if ( empty( $img_src ) ) {
				$theme = wp_get_theme();
				$img_src = $theme->get_screenshot();
			}

			?>
			<div class="ocdi__gl-item js-ocdi-gl-item" data-categories="<?php echo esc_attr( Helpers::get_demo_import_item_categories( $import_file ) ); ?>" data-name="<?php echo esc_attr( strtolower( $import_file['import_file_name'] ) ); ?>">
				<div class="ocdi__gl-item-image-container">
					<?php if ( ! empty( $img_src ) ) : ?>
					<img class="ocdi__gl-item-image" src="<?php echo esc_url( $img_src ) ?>">
					<?php else : ?>
					<div class="ocdi__gl-item-image  ocdi__gl-item-image--no-image"><?php esc_html_e( 'No preview image.', 'mb_framework' ); ?></div>
					<?php endif; ?>
				</div>
				<div class="ocdi__gl-item-footer<?php echo ! empty( $import_file['preview_url'] ) ? '  ocdi__gl-item-footer--with-preview' : ''; ?>">
					<h4 class="ocdi__gl-item-title" title="<?php echo esc_attr( $import_file['import_file_name'] ); ?>"><?php echo esc_html( $import_file['import_file_name'] ); ?></h4>
					<button class="ocdi__gl-item-button  button  button-primary  js-ocdi-gl-import-data" value="<?php echo esc_attr( $index ); ?>"><?php esc_html_e( 'Import', 'mb_framework' ); ?></button>
					<?php if ( ! empty( $import_file['preview_url'] ) ) : ?>
					<a class="ocdi__gl-item-button  button" href="<?php echo esc_url( $import_file['preview_url'] ); ?>" target="_blank"><?php esc_html_e( 'Preview', 'mb_framework' ); ?></a>
					<?php endif; ?>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>

	<div id="js-ocdi-modal-content"></div>

	<?php endif;
	endif; ?>
	
	<p class="ocdi__ajax-loader  js-ocdi-ajax-loader">
		<span class="spinner"></span> <?php esc_html_e( 'Importing, please wait!', 'mb_framework' ); ?>
	</p>

	<div class="ocdi__response  js-ocdi-ajax-response"></div>
</div>
