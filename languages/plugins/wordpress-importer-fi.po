# Translation of Plugins - WordPress Importer - Stable (latest release) in Finnish
# This file is distributed under the same license as the Plugins - WordPress Importer - Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2015-08-01 21:41:45+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/2.4.0-alpha\n"
"Language: fi\n"
"Project-Id-Version: Plugins - WordPress Importer - Stable (latest release)\n"

#: parsers/class-wxr-parser.php:43
msgid "Details are shown above. The importer will now try again with a different parser..."
msgstr "Tiedot näkyvät yläpuolella. Tuontityökalu yrittää uudelleen eri parserilla..."

#: parsers/class-wxr-parser-simplexml.php:29
#: parsers/class-wxr-parser-simplexml.php:37 parsers/class-wxr-parser.php:42
msgid "There was an error when reading this WXR file"
msgstr "WXR-tiedoston lukemisessa tapahtui virhe"

#: parsers/class-wxr-parser-regex.php:96 parsers/class-wxr-parser-xml.php:48
#: parsers/class-wxr-parser-simplexml.php:41
#: parsers/class-wxr-parser-simplexml.php:46
msgid "This does not appear to be a WXR file, missing/invalid WXR version number"
msgstr "Tämä ei vaikuta olevan WXR-tiedosto, WXR-versionumero puuttuu tai on virheellinen"

#: class-wp-import.php:106 class-wp-import.php:115 class-wp-import.php:166
#: class-wp-import.php:170 class-wp-import.php:179
msgid "Sorry, there has been an error."
msgstr "Valitettavasti on tapahtunut virhe."

#: class-wp-import.php:107
msgid "The file does not exist, please try again."
msgstr "Tiedostoa ei ole olemassa, kokeile uudelleen."

#: class-wp-import.php:150
msgid "All done."
msgstr "Valmista."

#: class-wp-import.php:151
msgid "Remember to update the passwords and roles of imported users."
msgstr "Muista päivittää tuotujen käyttäjien salasanat ja roolit."

#: class-wp-import.php:150
msgid "Have fun!"
msgstr "Pidä hauskaa!"

#: class-wp-import.php:187
msgid "This WXR file (version %s) may not be supported by this version of the importer. Please consider updating."
msgstr "Tämä WXR-tiedosto (versio %s) ei välttämättä ole tuettu tässä tuontityökalun versiossa. Suosittelemme päivittämään."

#: class-wp-import.php:212
msgid "Failed to import author %s. Their posts will be attributed to the current user."
msgstr "Käyttäjän %s tuonti ei onnistunut. Hänen kirjoituksensa merkitään nykyiselle käyttäjälle."

#: class-wp-import.php:238
msgid "Assign Authors"
msgstr "Määritä kirjoittajat"

#: class-wp-import.php:241
msgid "If a new user is created by WordPress, a new password will be randomly generated and the new user&#8217;s role will be set as %s. Manually changing the new user&#8217;s details will be necessary."
msgstr "Jos WordPress luo uuden käyttäjän, tälle luodaan satunnainen salasana ja käyttäjän rooliksi asetetaan %s. Käyttäjän tietojen muokkaaminen käsin on tarpeen."

#: class-wp-import.php:251
msgid "Import Attachments"
msgstr "Tuo liitteet"

#: class-wp-import.php:254
msgid "Download and import file attachments"
msgstr "Lataa ja tuo liitetiedostot"

#: class-wp-import.php:258
msgid "Submit"
msgstr "Lähetä"

#: class-wp-import.php:271
msgid "Import author:"
msgstr "Tuo kirjoittaja:"

#: class-wp-import.php:286
msgid "as a new user:"
msgstr "uutena käyttäjänä:"

#: class-wp-import.php:283
msgid "or create new user with login name:"
msgstr "tai luo uusi käyttäjä tunnuksella:"

#: class-wp-import.php:296
msgid "assign posts to an existing user:"
msgstr "aseta kirjoittajaksi olemassa oleva käyttäjä:"

#: class-wp-import.php:306
msgid "- Select -"
msgstr "- Valitse -"

#: class-wp-import.php:298
msgid "or assign posts to an existing user:"
msgstr "tai aseta kirjoittajaksi olemassa oleva käyttäjä:"

#: class-wp-import.php:360
msgid "Failed to create new user for %s. Their posts will be attributed to the current user."
msgstr "Uuden käyttäjän %s luominen epäonnistui. Sisällöt merkitään nykyiselle käyttäjälle."

#: class-wp-import.php:457
msgid "Failed to import post tag %s"
msgstr "Avainsanan %s tuonti epäonnistui"

#: class-wp-import.php:412
msgid "Failed to import category %s"
msgstr "Kategorian %s tuonti epäonnistui"

#: class-wp-import.php:512 class-wp-import.php:738
msgid "Failed to import %s %s"
msgstr "Tuonti epäonnistui: %s %s"

#: class-wp-import.php:605
msgid "Failed to import &#8220;%s&#8221;: Invalid post type %s"
msgstr "Sisällön &#8221;%s&#8221; tuonti epäonnistui: virheellinen sisältötyyppi %s"

#: class-wp-import.php:878
msgid "Menu item skipped due to missing menu slug"
msgstr "Valikkokohde ohitettiin puuttuvan polkutunnuksen vuoksi"

#: class-wp-import.php:704
msgid "Failed to import %s &#8220;%s&#8221;"
msgstr "Tuonti epäonnistui: %s &#8221;%s&#8221;"

#: class-wp-import.php:642
msgid "%s &#8220;%s&#8221; already exists."
msgstr "%s &#8221;%s&#8221; on jo olemassa."

#: class-wp-import.php:885
msgid "Menu item skipped due to invalid menu slug: %s"
msgstr "Valikkokohde ohitettiin virheellisen polkutunnuksen vuoksi: %s"

#: class-wp-import.php:948
msgid "Fetching attachments is not enabled"
msgstr "Liitetiedostojen nouto ei ole käytössä"

#: class-wp-import.php:961
msgid "Invalid file type"
msgstr "Tiedoston tyyppi ei kelpaa."

#: class-wp-import.php:1047
msgid "Remote server did not respond"
msgstr "Etäpalvelin ei vastaa"

#: class-wp-import.php:1065
msgid "Remote file is too large, limit is %s"
msgstr "Tiedosto on liian suuri, raja on %s."

#: class-wp-import.php:1054
msgid "Zero size file downloaded"
msgstr "Tyhjä tiedosto ladattu"

#: class-wp-import.php:1231
msgid "A new version of this importer is available. Please update to version %s to ensure compatibility with newer export files."
msgstr "Uusi versio tästä tuontityökalusta on saatavilla. Päivitä versioon %s varmistaaksesi yhteensopivuuden uudempien vientitiedostojen kanssa."

#: class-wp-import.php:1224
msgid "Import WordPress"
msgstr "WordPress-tuonti"

#: class-wp-import.php:1246
msgid "Howdy! Upload your WordPress eXtended RSS (WXR) file and we&#8217;ll import the posts, pages, comments, custom fields, categories, and tags into this site."
msgstr "Moikka! Lataa WordPressin eXtended RSS (WXR)-tiedostosi ja tuontiohjelma tuo WordPress-blogistasi kaikki artikkelit, sivut, kommentit, mukautetut kentät, kategoriat ja avainsanat tälle sivustolle."

#: class-wp-import.php:1247
msgid "Choose a WXR (.xml) file to upload, then click Upload file and import."
msgstr "Valitse WXR (.xml) -tiedosto, napsauta sitten Siirrä tiedosto."

#: wordpress-importer.php:58
msgid "Import <strong>posts, pages, comments, custom fields, categories, and tags</strong> from a WordPress export file."
msgstr "Tuo <strong>artikkelit, sivut, kommentit, avainkentät, kategoriat ja avainsanat</strong> WordPress-vientitiedostosta."

#. Plugin Name of the plugin
msgid "WordPress Importer"
msgstr "WordPress-tuontiohjelma"

#. Description of the plugin
msgid "Import posts, pages, comments, custom fields, categories, tags and more from a WordPress export file."
msgstr "Tuo artikkelit, sivut, kommentit, avainkentät, kategoriat, avainsanat ja muut WordPress-vientitiedostosta."

#. Author of the plugin
msgid "wordpressdotorg"
msgstr "wordpresspisteorg"